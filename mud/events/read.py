# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2

class ReadEvent(Event2):
    NAME = "read"

    def get_event_templates(self):
        return self.object.get_event_templates()

    def perform(self):
        if not self.actor.can_see():
            return self.failed_cannot_see()
        self.buffer_clear()
        self.buffer_inform("read.actor", object=self.object)
        return self.actor.send_result(self.buffer_get())

        self.actor.send_result(self.buffer_get())

    def failed_cannot_see(self):
        self.fail()
        self.buffer_clear()
        self.buffer_inform("look.failed")
        self.actor.send_result(self.buffer_get())

    
