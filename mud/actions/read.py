# -*- coding: utf-8 -*-

from .action import Action2
from mud.events import ReadEvent

class ReadAction(Action2):
    EVENT = ReadEvent
    RESOLVE_OBJECT = "resolve_for_take"
    ACTION = "read"
